package com.example.test.tofu.app

import cats.{Id, ~>}
import com.mockrunner.mock.jdbc.MockResultSet
import doobie.Meta
import io.circe.Json
import io.circe.syntax._
import org.specs2.mutable.Specification

import scala.jdk.CollectionConverters._

class FetchServiceSpec extends Specification {
  val details: Json = Map("a" -> "b").asJson

  val mockedRepo: FetchRepository[Id] = new FetchRepository[Id] {
    def fetch[A: Meta]: Id[MyModel[A]] = {
      MyModel(1, "a", castValue(details.noSpaces))
    }

    private def castValue[A](strValue: String)(implicit M: Meta[A]): A = {
      val rs = new MockResultSet("mr")
      rs.addColumn("details", List(strValue).asJavaCollection.toArray)
      rs.next()
      M.get.unsafeGetNonNullable(rs, 1)
    }
  }

  "FetchService should" >> {
    "fetch" >> {
      implicit val xa: Id ~> Id = λ[Id ~> Id](identity(_))
      val service = FetchService.create(mockedRepo)

      service.fetchModel mustEqual MyModel(1, "a", details)
    }
  }
}
