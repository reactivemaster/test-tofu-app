package com.example.test.tofu.app

import doobie.Meta

trait FetchRepository[DB[_]] {
  def fetch[A: Meta]: DB[MyModel[A]]
}
