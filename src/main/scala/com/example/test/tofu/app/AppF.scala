package com.example.test.tofu.app

import cats.effect.{ConcurrentEffect, Sync, Timer}
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.~>
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import tofu.HasLocal

class AppF[F[_]: Timer: ConcurrentEffect, G[_]: Sync: HasLocal[*[_], String]](
    implicit fkFG: F ~> G,
    fkGF: G ~> F) {
  def create: F[Unit] = {
    fkGF(for {
      logger <- Slf4jLogger.create[G]
      _ <- logger.info("Starting Application")
      serviceWirings <- ServiceWirings.create[G]
      endpointWirings = EndpointWirings.create[F, G](serviceWirings)
      _ <- fkFG(endpointWirings.launchHttpService.compile.drain)
    } yield ())
  }
}
