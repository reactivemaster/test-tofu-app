package com.example.test.tofu.app

import cats.Monad
import cats.effect.Sync
import cats.syntax.flatMap._
import cats.syntax.functor._
import io.chrisdavenport.log4cats.Logger
import io.chrisdavenport.log4cats.slf4j.Slf4jLogger
import tofu.HasContext

trait MyService[F[_]] {
  def double(s: String): F[String]
}

object MyService {
  def create[F[_]: Sync: HasContext[*[_], String]]: F[MyService[F]] = {
    Slf4jLogger.create[F].map { logger =>
      new Impl[F](logger)
    }
  }

  private final class Impl[F[_]: Monad](logger: Logger[F])(
      implicit H: HasContext[F, String])
      extends MyService[F] {
    def double(s: String): F[String] = {
      for {
        requestId <- H.context
        _ <- logger.info(s"[$requestId] Input string is $s")
        res = s * 2
        _ <- logger.info(s"[$requestId] Output string is $res")
      } yield res
    }
  }
}
