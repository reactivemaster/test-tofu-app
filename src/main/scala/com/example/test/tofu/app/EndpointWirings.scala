package com.example.test.tofu.app

import cats.effect.{ConcurrentEffect, ExitCode, Sync, Timer}
import cats.~>
import fs2.Stream
import org.http4s.HttpRoutes
import org.http4s.implicits._
import org.http4s.server.blaze.BlazeServerBuilder
import tofu.HasLocal

import scala.concurrent.ExecutionContext

final class EndpointWirings[F[_]: Timer: ConcurrentEffect](
    routes: HttpRoutes[F]) {
  def launchHttpService: Stream[F, ExitCode] =
    BlazeServerBuilder
      .apply(ExecutionContext.global)
      .bindHttp(8080, "0.0.0.0")
      .withHttpApp(routes.orNotFound)
      .serve
}

object EndpointWirings {
  def create[F[_]: Timer: ConcurrentEffect, G[_]: Sync: HasLocal[*[_], String]](
      serviceWirings: ServiceWirings[G])(
      implicit fkGF: G ~> F): EndpointWirings[F] = {
    val routes = Routes
      .create[F, G](serviceWirings)
      .routes
    new EndpointWirings[F](routes)
  }
}
