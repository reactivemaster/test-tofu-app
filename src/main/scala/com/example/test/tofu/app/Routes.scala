package com.example.test.tofu.app

import java.util.UUID

import cats.effect.Sync
import cats.syntax.flatMap._
import cats.syntax.functor._
import cats.~>
import org.http4s._
import org.http4s.dsl.Http4sDsl
import tofu.HasLocal

trait Routes[F[_]] {
  val routes: HttpRoutes[F]
}

object Routes {
  def create[F[_]: Sync, G[_]: Sync: HasLocal[*[_], String]](
      serviceWirings: ServiceWirings[G])(implicit fkGF: G ~> F): Routes[F] =
    new Impl[F, G](serviceWirings)

  private final class Impl[F[_]: Sync, G[_]: Sync: HasLocal[*[_], String]](
      serviceWirings: ServiceWirings[G])(implicit fkGF: G ~> F)
      extends Http4sDsl[F]
      with Routes[F] {
    import serviceWirings._

    val routes: HttpRoutes[F] = HttpRoutes.of {
      case GET -> Root / "test" =>
        val res = for {
          requestId <- Sync[G].delay(UUID.randomUUID()).map(_.toString)
          res <- HasLocal[G, String].local(myService.double("hello"))(_ =>
            requestId)
        } yield res

        fkGF(res).flatMap(Ok(_))
    }
  }
}
