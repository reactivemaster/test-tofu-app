package com.example.test.tofu.app

import cats.~>
import cats.syntax.either._
import doobie.util.meta.Meta
import io.circe.Json
import io.circe.parser.parse

trait FetchService[F[_]] {
  def fetchModel: F[MyModel[Json]]
}

object FetchService {

  implicit val jsonMeta: Meta[Json] =
    Meta[String].imap(s => parse(s).leftMap(err => throw err).merge)(_.noSpaces)

  def create[F[_], DB[_]](repo: FetchRepository[DB])(
      implicit xa: DB ~> F): FetchService[F] = {
    new Impl[F, DB](repo)
  }

  private final class Impl[F[_], DB[_]](repo: FetchRepository[DB])(
      implicit xa: DB ~> F)
      extends FetchService[F] {
    def fetchModel: F[MyModel[Json]] = {
      xa(repo.fetch[Json])
    }
  }
}
