package com.example.test.tofu.app

final case class MyModel[A](id: Int, name: String, details: A)
