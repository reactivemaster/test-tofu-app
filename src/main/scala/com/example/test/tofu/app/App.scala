package com.example.test.tofu.app

import cats.data.ReaderT
import cats.effect.ExitCode
import cats.syntax.functor._
import cats.~>
import monix.eval.{Task, TaskApp}

object App extends TaskApp {
  implicit def wrapToCtx[F[_]]: F ~> ReaderT[F, String, *] =
    λ[F ~> ReaderT[F, String, *]] { r =>
      ReaderT.liftF(r)
    }

  implicit def applyCtx[F[_]]: ReaderT[F, String, *] ~> F =
    λ[ReaderT[F, String, *] ~> F] { r =>
      r.run("")
    }

  def run(args: List[String]): Task[ExitCode] = {
    val appF = new AppF[Task, ReaderT[Task, String, *]]
    appF.create.flatMap(_ => Task.never).as(ExitCode.Success)
  }
}
