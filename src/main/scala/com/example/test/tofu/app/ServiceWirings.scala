package com.example.test.tofu.app

import cats.effect.Sync
import cats.syntax.functor._
import tofu.HasContext

final case class ServiceWirings[F[_]](myService: MyService[F])

object ServiceWirings {
  def create[F[_]: Sync: HasContext[*[_], String]]: F[ServiceWirings[F]] = {
    MyService.create[F].map(ServiceWirings(_))
  }
}
