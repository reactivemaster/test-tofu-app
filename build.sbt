val Http4sVersion = "0.21.4"
val CirceVersion = "0.13.0"
val Specs2Version = "4.9.3"
val LogbackVersion = "1.2.3"
val Log4catsVersion = "1.0.1"
val TofuVersion = "0.7.7"
val MonixVersion = "3.0.0"
val DoobieVersion = "0.9.0"
val MockrunnerVersion = "2.0.4"

lazy val root = (project in file("."))
  .settings(
    organization := "com.example",
    name := "test-tofu-app",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.13.1",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s"      %% "http4s-blaze-client" % Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
      "io.circe"        %% "circe-generic"       % CirceVersion,
      "io.circe"       %% "circe-parser"         % CirceVersion,
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion,
      "io.chrisdavenport" %% "log4cats-core"     % Log4catsVersion,
      "io.chrisdavenport" %% "log4cats-slf4j"    % Log4catsVersion,
      "ru.tinkoff" %% "tofu-core"                % TofuVersion,
      "io.monix" %% "monix"                      % MonixVersion,
      "org.tpolecat" %% "doobie-core"            % DoobieVersion,
      "org.specs2"      %% "specs2-core"         % Specs2Version % "test",
      "com.mockrunner" % "mockrunner-jdbc"       % MockrunnerVersion % "test"
    ),
    addCompilerPlugin("org.typelevel" %% "kind-projector"     % "0.11.0" cross CrossVersion.patch),
    addCompilerPlugin("com.olegpy"    %% "better-monadic-for" % "0.3.1")
  )

scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Xfatal-warnings",
)
